﻿using UnityEngine;
using System.Collections.Generic;

namespace NovelMaker
{
    [System.Serializable]
    public class NovelPageData
    {
        public List<NovelCommandData> commandDataList;
    }

	[System.Serializable]
	public class NovelCommandData{
        public string command;
		public string value = "";
        public string arg1;
        public string arg2;

        public Object objectReference;
	}
}