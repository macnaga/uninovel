﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NovelMaker
{
    [System.Serializable]
    public class NovelCommandUnityEvent : UnityEvent<NovelCommandData> { }

    public class NovelCommandHandler : MonoBehaviour
    {
        public NovelCommandUnityEvent onCommand;

        public void Handle(NovelContext context, NovelCommandData data)
        {
            var ui = context.UI;
            switch (data.command)
            {
                case "bg":
                    ui.bgImage.texture = data.objectReference as Texture;
                    ui.bgImage.color = Color.white;
                    ui.bgImage.enabled = true;
                    context.Proceed();
                    break;
                case "bg_off":
                    ui.bgImage.enabled = false;
                    context.Proceed();
                    break;
                case "left_character":
                    ui.leftCharacter.texture = data.objectReference as Texture;
                    ui.leftCharacter.color = Color.white;
                    ui.leftCharacter.enabled = true;
                    context.Proceed();
                    break;
                case "left_character_off":
                    ui.leftCharacter.enabled = false;
                    context.Proceed();
                    break;
                case "right_character":
                    ui.rightCharacter.texture = data.objectReference as Texture;
                    ui.rightCharacter.color = Color.white;
                    ui.rightCharacter.enabled = true;
                    context.Proceed();
                    break;
                case "right_character_off":
                    ui.rightCharacter.enabled = false;
                    context.Proceed();
                    break;
                case "":
                    ui.messageBox.SetNameText(data.arg1);
                    ui.messageBox.SetText(data.value);
                    break;
                default:
                    onCommand.Invoke(data);
                    break;
            }
        }
    }
}