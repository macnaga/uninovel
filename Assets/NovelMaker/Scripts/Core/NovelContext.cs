﻿using System.Collections.Generic;
using UnityEngine.Events;

namespace NovelMaker
{
    public class NovelContext
    {
        int index;
        NovelCommandHandler commandHandler;
        UnityEvent onFinish;
        public NovelUI UI { get; private set; }
        public List<NovelCommandData> CommandDataList { get; private set; } = new List<NovelCommandData>();
        public List<NovelCommandData> Log => CommandDataList.GetRange(0, index);

        public NovelContext(List<NovelCommandData> commandDataList, NovelCommandHandler handler, NovelUI ui, UnityEvent onFinish)
        {
            CommandDataList = commandDataList;
            commandHandler = handler;
            UI = ui;
            this.onFinish = onFinish;
        }

        public void Proceed()
        {
            if (CommandDataList.Count <= index)
            {
                onFinish.Invoke();
                return;
            }
            var command = CommandDataList[index++];
            commandHandler.Handle(this, command);
        }
    }
}