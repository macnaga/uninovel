﻿using System.Collections.Generic;
using UnityEngine;

namespace NovelMaker
{
    [CreateAssetMenu()]
    public class NovelDataAsset : ScriptableObject
    {
        public List<NovelCommandData> commandDataList = new List<NovelCommandData>();

        public string ToJson()
        {
            return JsonUtility.ToJson(this);
        }
    }
}