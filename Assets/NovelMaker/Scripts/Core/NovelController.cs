﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace NovelMaker
{
    public class NovelController : MonoBehaviour
    {
        public UnityEvent onFinish;
        public NovelCommandHandler commandHandler;
        public NovelUI ui;
        public NovelContext Context { get; private set; }

        void Awake()
        {
            ui.Initialize(this);
        }

        public void Play(string json)
        {
            var commandDataList = JsonUtility.FromJson<NovelPageData>(json).commandDataList;
            Context = new NovelContext(commandDataList, commandHandler, ui, onFinish);
            Context.Proceed();
        }

        public void Play(NovelDataAsset data)
        {
            Context = new NovelContext(data.commandDataList, commandHandler, ui, onFinish);
            Context.Proceed();
        }

        public void Proceed()
        {
            Context.Proceed();
        }
    }
}