﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace NovelMaker
{
    public class UIBackLog : MonoBehaviour
    {
        public NovelController novelController;
        public UIBackLogItem itemTemplate;
        public Transform contentRoot;

        List<UIBackLogItem> items = new List<UIBackLogItem>();

        void OnEnable()
        {
            foreach (var data in novelController.Context.Log)
            {
                if (data.command != string.Empty) { continue; }
                var item = Instantiate(itemTemplate, contentRoot, false);
                item.Set(data);
                items.Add(item);
                item.gameObject.SetActive(true);
            }
            itemTemplate.gameObject.SetActive(false);
        }

        void OnDisable()
        {
            foreach (var item in items)
            {
                Destroy(item.gameObject);
            }
            items.Clear();
        }

        public void OnClickClose()
        {
            this.gameObject.SetActive(false);
        }
    }
}