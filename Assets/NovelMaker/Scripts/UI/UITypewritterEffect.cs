﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UITypewritterEffect : MonoBehaviour
{
    public float charsPerSecond = 0.2f;
    string words;
    float timer;
    Text text;
    int currentPos = 0;
    public bool IsActive { get; private set; } = false;

    void Start()
    {
        timer = 0;
        text = GetComponent<Text>();
        text.text = "";
    }

    void Update()
    {
        OnStartWriter();
    }

    public void StartEffect(string text)
    {
        IsActive = true;
        words = text;
    }

    public void Finish()
    {
        IsActive = false;
        timer = 0;
        currentPos = 0;
        text.text = words;
    }

    void OnStartWriter()
    {
        if (IsActive)
        {
            timer += Time.deltaTime;
            if (timer >= charsPerSecond)
            {
                timer = 0;
                currentPos++;
                text.text = words.Substring(0, currentPos);
                if (currentPos >= words.Length)
                {
                    Finish();
                }
            }
        }
    }
}