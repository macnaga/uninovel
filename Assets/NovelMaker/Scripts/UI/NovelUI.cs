﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace NovelMaker
{
    public class NovelUI : MonoBehaviour
    {
        public RawImage bgImage;
        public RawImage leftCharacter;
        public RawImage rightCharacter;
        public UIMessageBox messageBox;
        public UIBackLog backLog;
        NovelController controller;

        public void Initialize(NovelController controller)
        {
            this.controller = controller;
        }

        #region Inspector
        public void OnClickMessageBox()
        {
            if (messageBox.IsTypewriting)
            {
                messageBox.FinishTypewritting();
                return;
            }
            controller.Proceed();
        }

        public void OnClickLog()
        {
            backLog.gameObject.SetActive(true);
        }
        #endregion
    }
}