﻿using UnityEngine;
using UnityEngine.UI;

namespace NovelMaker
{
    public class UIBackLogItem : MonoBehaviour
    {
        public Text label;
        public Text nameLabel;

        public void Set(NovelCommandData data)
        {
            if (data.command == string.Empty)
            {
                label.text = data.value;
                nameLabel.transform.parent.gameObject.SetActive(!string.IsNullOrEmpty(data.arg1));
                nameLabel.text = data.arg1;
            }
        }
    }
}