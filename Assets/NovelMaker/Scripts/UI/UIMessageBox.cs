﻿using UnityEngine;
using UnityEngine.UI;

namespace NovelMaker
{
    public class UIMessageBox : MonoBehaviour
    {
        public Text label;
        public Text nameLabel;
        public UITypewritterEffect typewritterEffect;
        public bool IsTypewriting => typewritterEffect.IsActive;

        public void SetText(string text)
        {
            if (typewritterEffect != null)
            {
                typewritterEffect.StartEffect(text);
            }
            else
            {
                label.text = text;
            }
        }

        public void FinishTypewritting()
        {
            typewritterEffect.Finish();
        }

        public void SetNameText(string name)
        {
            nameLabel.transform.parent.gameObject.SetActive(!string.IsNullOrEmpty(name));
            nameLabel.text = name;
        }
    }
}