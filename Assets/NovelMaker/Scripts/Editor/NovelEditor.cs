﻿using UnityEngine;
using UnityEditor;

namespace NovelMaker
{
    [CustomEditor(typeof(NovelDataAsset))]
    public class NovelEditor : Editor
    {
        string[] commands = new string[]
        {
            "",
            "bg",
            "bg_off",
            "left_character",
            "left_character_off",
            "right_character",
            "right_character_off",
        };
        Vector2 scrollPosition = Vector2.zero;
        int selectedIndex;

        public override void OnInspectorGUI()
        {
            EditorGUILayout.BeginHorizontal("box");
            {
                selectedIndex = EditorGUILayout.Popup(selectedIndex, commands);
                if (GUILayout.Button("+"))
                {
                    NovelCommandData data = new NovelCommandData();
                    data.command = commands[selectedIndex];
                    (target as NovelDataAsset).commandDataList.Add(data);
                }
            }
            EditorGUILayout.EndHorizontal();

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
            var commandDataList = (target as NovelDataAsset).commandDataList;
            foreach (var data in commandDataList)
            {
                EditorGUILayout.BeginHorizontal("box");
                if (GUILayout.Button("-", GUILayout.Width(50)))
                {
                    commandDataList.Remove(data);
                    break;
                }
                data.command = EditorGUILayout.TextField(data.command, GUILayout.Width(120f));
                DrawCommandData(data);
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.EndScrollView();

            if (GUILayout.Button("Json"))
            {
                Debug.Log((target as NovelDataAsset).ToJson());
            }

            EditorUtility.SetDirty(target);
        }

        void DrawCommandData(NovelCommandData data)
        {
            switch (data.command)
            {
                case "bg":
                    data.objectReference = EditorGUILayout.ObjectField(data.objectReference, typeof(Texture), false, GUILayout.Width(50f), GUILayout.Height(50f));
                    break;
                case "right_character":
                case "left_character":
                    data.objectReference = EditorGUILayout.ObjectField(data.objectReference, typeof(Texture), false, GUILayout.Width(50f), GUILayout.Height(50f));
                    break;
                case "":
                    data.arg1 = EditorGUILayout.TextField(data.arg1, GUILayout.Width(100f));
                    // EditorGUILayout.BeginHorizontal();
                    {
                        data.value = EditorGUILayout.TextArea(data.value);//, GUILayout.Width(Screen.width * 2 / 3f));
                    }
                    // EditorGUILayout.EndHorizontal();
                    break;
            }
        }
    }
}