﻿using UnityEngine;
using NovelMaker;

public class Example : MonoBehaviour
{
    public NovelController novelController;
    public bool playOnStart = true;
    public NovelDataAsset asset;

    void Start()
    {
        novelController.gameObject.SetActive(false);
        if (playOnStart)
        {
            Play();
        }
    }

    void Play()
    {
        novelController.gameObject.SetActive(true);
        novelController.Play(asset);
    }

    #region NovelController UnityEvent
    public void OnCommand(NovelCommandData data)
    {

    }

    public void OnFinish()
    {
        novelController.gameObject.SetActive(false);
    }
    #endregion
}
